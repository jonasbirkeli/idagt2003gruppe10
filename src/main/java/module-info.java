module proggang {
  requires javafx.controls;
  requires javafx.fxml;
  requires java.logging;
  requires java.desktop;

  exports backend.core;
  exports backend.transforms;
  exports backend.geometry;
  exports backend.models;

  exports frontend;
  exports frontend.controllers;

  opens backend.models;
  opens frontend.controllers;
  opens backend.geometry;

  opens css;
  exports backend.utility.state;
}